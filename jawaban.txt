1.buat Database

 

create database myshop;

 

2.

Tabel users
 create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Tabel categories
 create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

Tabel items
 create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(20),
    -> categories_id int(8),
    -> foreign key(categories_id) references categories(id)
    -> );

3.

Tabel users
insert into users(name,email,password) values("John Doe", "jhon@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

Tabel categories 
insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

Tabel items
insert into items(name, description, price, stock, categories_id) values("Sumsangb50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Unikloh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4.

Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.
select id, name, email from users;

Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
select * from items where price>1000000;

Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
select * from items where name like '%Watch';

Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items (gunakan join). Berikut contoh tampilan data yang ingin didapatkan
select items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name as kategori from items inner join categories on items.categories_id = categories.id;
5.

Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5.
update items set price = 2500000 where id = 1;